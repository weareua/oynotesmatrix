import regex as re
from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_bot_api.mregex_handler import MRegexHandler
from matrix_bot_api.mcommand_handler import MCommandHandler
from config import USERNAME, PASSWORD, SERVER
from components.engine import Engine
from components.services import RecordService
from components.help import basic_help_text, advanced_help_text


def proceed_request_callback(room, event):
    engine = Engine()
    engine.prepare_incoming_data(room, event)

    # if help
    if engine.category.upper() in ['ХЕЛП','HELP','ПОМОЩЬ', 'ДОПОМОГА', 'ІНСТРУКЦІЯ','ИНСТРУКЦИЯ']:
        # if advanced help
        if engine.text.upper() in ['ДОДАТКОВО', 'ADVANCED', 'ДОПОЛНИТЕЛЬНО']:
            engine.send(advanced_help_text)
        else:
            engine.send(basic_help_text)
    # add record or cat
    elif re.match(r'(?i)(^\+)\s?(.*)\s?', engine.text):
        record_raw = re.match(r'(?i)(^\+)(.*)\s?', engine.text).groups()[1].strip()
        record = RecordService.add(engine.chat_id, record_raw, engine.category)
        if engine.category:
            engine.send(
            "Запис <b>№{0}</b> додано в категорію <b>{1}</b>.".format(
                record.record_id,record.category))
        else:
            engine.send("Запис <b>№{0}</b> додано.".format(record.record_id))
    # delete record or cat
    elif re.match(r'(?i)(^\-)', engine.text):
        if engine.category and re.match(r'(?i)(^\-$)', engine.text):
            result = RecordService.delete_category(engine.chat_id, engine.category)
            if result:
                engine.send("Видаленo всі записи з категорією <b>{0}</b>".format(engine.category))
            else:
                engine.send("Не знайдено записів з категорією <b>{0}</b>".format(engine.category))
        else:
            id = re.match(r'(?i)(^\-)\s?(\d+)', engine.text).groups()[1].strip()
            RecordService.remove(engine.chat_id, id)
            engine.send("Запис <b>№{0}</b> видалено.".format(id))
    # edit record
    elif re.match(r'(?i)(^\=)\s?(\d+)\s?(.*)\s?', engine.text):
        if re.match(r'(?i)(^\=)\s?(\d+)\s?(\#)\s?(\S+)\s?', engine.text):
            records_raw = re.match(r'(?i)(^\=)\s?(\d+)\s?(\#)\s?(\S+)\s?', engine.text).groups()
            record = RecordService.edit_category(engine.chat_id, records_raw[1],records_raw[3])
            engine.send("Запис <b>№{0}</b> перенесено до категорії {1}".format(record.record_id, record.category))
        else:
            record_raw = re.match(r'(?i)(^\=)\s?(\d+)\s?(.*)\s?', engine.text).groups()
            record = RecordService.edit(engine.chat_id, record_raw[1],record_raw[2])
            engine.send("Запис <b>№{0}</b> оновлено.".format(record.record_id))
    # edit category
    elif re.match(r'(?i)(\={1})\s*(\S+)', engine.text):
        new_cat = re.match(r'(?i)(\={1})\s*(\S+)', engine.text).groups()[1]
        RecordService.change_category(engine.chat_id, engine.category, new_cat)
        engine.send("Категорія <b>{0}</b> тепер має назву <b>{1}</b>.".format(engine.category, new_cat))
    # mark record as completed
    elif re.match(r'(?i)(^\!)\s?(\d+)', engine.text):
        id = re.match(r'(?i)(^\!)\s?(\d+)\s?(.*)\s?', engine.text).groups()[1].strip()
        record = RecordService.complete(engine.chat_id, id)
        engine.send("Запис <b>№{0}</b> позначено завершеним.".format(record.record_id))
    #get record by id
    elif re.match(r'(?i)(^[0-9]+)', engine.text) or re.match(r'(?i)(^[0-9]+)', engine.category):
        try:
            id = int(re.match(r'(?i)(^\d*)', engine.text).groups()[0])
        except ValueError:
            id = int(re.match(r'(?i)(^\d*)', engine.category).groups()[0])
        record = RecordService.show_one_by_id(engine.chat_id, id)
        if record:
            engine.send("<b>{0}.</b> {1}".format(record.record_id, record.engine.text))
        else:
            engine.send("Жодних записів за цим запитом.")
    #show block
    elif re.match(r'(?i)(^\*)', engine.text):
        try:
            # show all active
            if re.match(r'(?i)(^\*\s?\+)', engine.text):
                # show active in range
                if re.match(r'(?i)(^\*\s?\+)\s?(\d*)\:(\d*)', engine.text):
                    raw_groups = re.match(r'(?i)(^\*\s?\+)\s?(\d*)\:(\d*)', engine.text).groups()
                    start_id = raw_groups[1]
                    stop_id = raw_groups[2]
                    if start_id and stop_id:
                        records = RecordService.show_active_in_range(
                            engine.chat_id, int(start_id), int(stop_id), engine.category)
                    elif start_id:
                        records = RecordService.show_last_n_active(
                            engine.chat_id, int(start_id), engine.category)
                    elif stop_id:
                        records = RecordService.show_first_n_active(
                            engine.chat_id, int(stop_id), engine.category)

                    message = engine.prepare_records_list_message(records)
                    engine.send(message)
                else:
                    records = RecordService.show_active(engine.chat_id, engine.category)
                    message = engine.prepare_records_list_message(records)
                    engine.send(message)
            # show all completed
            elif re.match(r'(?i)(^\*\s?\!)', engine.text):
                # show completed in range
                if re.match(r'(?i)(^\*\s?\!)\s?(\d*)\:(\d*)', engine.text):
                    raw_groups = re.match(r'(?i)(^\*\s?\!)\s?(\d*)\:(\d*)', engine.text).groups()
                    start_id = raw_groups[1]
                    stop_id = raw_groups[2]
                    if start_id and stop_id:
                        records = RecordService.show_inactive_in_range(
                            engine.chat_id, int(start_id), int(stop_id), engine.category)
                    elif start_id:
                        records = RecordService.show_last_n_inactive(
                            engine.chat_id, int(start_id), engine.category)
                    elif stop_id:
                        records = RecordService.show_first_n_inactive(
                            engine.chat_id, int(stop_id), engine.category)
                    message = engine.prepare_records_list_message(records)
                    engine.send(message)
                else:
                    records = RecordService.show_inactive(engine.chat_id, engine.category)
                    message = engine.prepare_records_list_message(records)
                    engine.send(message)
            # show all in range
            elif re.match(r'(?i)(^\*)\s?(\d*)\:(\d*)', engine.text):
                raw_groups = re.match(r'(?i)(^\*)\s?(\d*)\:(\d*)', engine.text).groups()
                start_id = raw_groups[1]
                stop_id = raw_groups[2]
                if start_id and stop_id:
                    records = RecordService.show_in_range(
                        engine.chat_id, int(start_id), int(stop_id), engine.category)
                elif start_id:
                    records = RecordService.show_last_n(
                        engine.chat_id, int(start_id), engine.category)
                elif stop_id:
                    records = RecordService.show_first_n(
                        engine.chat_id, int(stop_id), engine.category)
                message = engine.prepare_records_list_message(records, mark_completed=True)
                engine.send(message)
            elif re.match(r'(?i)(^\*\s?\#$)', engine.text):
                categories = RecordService.get_all_categories(engine.chat_id)
                message = engine.prepare_categories_list_message(categories)
                engine.send(message)
            # else show all
            else:
                records = RecordService.show_all(engine.chat_id, engine.category)
                message = engine.prepare_records_list_message(records, mark_completed=True)
                engine.send(message)
        except IndexError:
            engine.send("Жодних записів за цим запитом.")
    
    # find block
    elif re.match(r'(?i)(^\?)(.*)\s?', engine.text):
        # find active with search string
        if re.match(r'(?i)(^\?\s?\+)\s?(\S*)\s?(.*)\s?', engine.text):
            keyword = re.match(r'(?i)(^\?\s?\+)\s?(\S*)\s?(.*)\s?', engine.text).groups()[1].strip()
            records = RecordService.find_active_by_keyword(engine.chat_id, keyword, engine.category)
            message = engine.prepare_records_list_message(
                records, keyword=keyword)
            engine.send(message)
        # find completed with search string
        elif re.match(r'(?i)(^\?\s?\!)\s?(\S*)\s?(.*)\s?', engine.text):
            keyword = re.match(r'(?i)(^\?\s?\!)\s?(\S*)\s?(.*)\s?', engine.text).groups()[1].strip()
            records = RecordService.find_inactive_by_keyword(engine.chat_id, keyword, engine.category)
            message = engine.prepare_records_list_message(
                records, keyword=keyword)
            engine.send(message)
        # find all with keyword
        else:
            keyword = re.match(r'(?i)(^\?)\s?(\S*)\s?(.*)\s?', engine.text).groups()[1].strip()
            records = RecordService.find_all_by_keyword(engine.chat_id, keyword, engine.category)
            message = engine.prepare_records_list_message(
                records, mark_completed=True, keyword=keyword)
            engine.send(message)
    

def main():
    # Create an instance of the MatrixBotAPI
    bot = MatrixBotAPI(USERNAME, PASSWORD, SERVER)

    
    proceed_request_handler = MRegexHandler(r'(?i)({Cyrillic}\d+|\S+)\s*(.*)', proceed_request_callback)
    bot.add_handler(proceed_request_handler)

    # Start polling
    bot.start_polling()

    # Infinitely read stdin to stall main thread while the bot runs in other threads
    while True:
        input()
