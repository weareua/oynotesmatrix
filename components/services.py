import logging
from pony.orm import db_session, desc, raw_sql, distinct, select
from components.models import Record


logging.basicConfig(
    format='%(asctime)s:%(message)s', filename='OYNotes.log',level=logging.ERROR)


class RecordService():
    @staticmethod
    def add(chat_id, record, category):
        try:
            record_id = RecordService.show_last_n(chat_id, 1, '')[0].record_id + 1
        except IndexError:
            record_id = 1
        try:
            with db_session:
                record = Record(
                    chat_id=chat_id, record_id=record_id, text=record, category=category)
        except Exception:
            logging.exception(
                "ERROR IN CREATE PROCESS FOR CHAT_ID: {0} AND RECORD_ID: {1}".format(
                    chat_id, record_id))
        return record

    @staticmethod
    def remove(chat_id, record_id):
        try:
            with db_session:
                record = RecordService.show_one_by_id(chat_id, record_id)
                record.delete()
        except Exception:
            logging.exception(
                "ERROR IN DELETE PROCESS FOR CHAT_ID: {0} AND RECORD_ID: {1}".format(
                    chat_id, record_id))
        return True

    @staticmethod
    def edit(chat_id, record_id, message):
        try:
            with db_session:
                record = RecordService.show_one_by_id(chat_id, record_id)
                record.text = message
        except Exception:
            logging.exception(
                "ERROR IN EDIT PROCESS FOR CHAT_ID: {0} AND RECORD_ID: {1}".format(
                    chat_id, record_id))
        return record

    @staticmethod
    def complete(chat_id, record_id):
        try:
            with db_session:
                record = RecordService.show_one_by_id(chat_id, record_id)
                record.active = False
        except Exception:
            logging.exception(
                "ERROR IN RECORD CREATE PROCESS FOR CHAT_ID: {0} AND RECORD_ID: {1}".format(
                    chat_id, record_id))
        return record

    @staticmethod
    def show_all(chat_id, category):
        with db_session:
            if category:
                records = Record.select(lambda r: r.chat_id == chat_id and r.category.upper() == category.upper())[:]
            else:
                records = Record.select(lambda r: r.chat_id == chat_id)[:]
        return records

    @staticmethod
    def find_all_by_keyword(chat_id, keyword, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id
                        and keyword.upper() in r.text.upper() and r.category.upper() == category.upper())[:]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and keyword.upper() in r.text.upper())[:]
        return records

    @staticmethod
    def find_active_by_keyword(chat_id, keyword, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id
                        and keyword.upper() in r.text.upper()
                        and r.active == True and r.category.upper() == category.upper())[:]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id
                        and keyword.upper() in r.text.upper() and r.active == True)[:]
        return records

    @staticmethod
    def find_inactive_by_keyword(chat_id, keyword, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and raw_sql(
                        search_query) and r.active == False and r.category.upper() == category.upper())[:]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and raw_sql(
                        search_query) and r.active == False)[:]
        return records
    
    @staticmethod
    def show_active(chat_id, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == True and r.category.upper() == category.upper())[:]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == True)[:]
        return records
    
    @staticmethod
    def show_inactive(chat_id, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == False and r.category.upper() == category.upper())[:]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == False)[:]
        return records

    @staticmethod
    def show_in_range(chat_id, start_id, stop_id, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.record_id > (
                        start_id-1) and r.record_id <= stop_id and r.category.upper() == category.upper())[:]
            else:
                records = Record.select(
                lambda r: r.chat_id == chat_id and r.record_id > (
                    start_id-1) and r.record_id <= stop_id)[:]
        return records

    @staticmethod
    def show_active_in_range(chat_id, start_id, stop_id, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == True and r.record_id > (
                        start_id-1) and r.record_id <= stop_id and r.category.upper() == category.upper())[:]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == True and r.record_id > (
                        start_id-1) and r.record_id <= stop_id)[:]
        return records

    @staticmethod
    def show_inactive_in_range(chat_id, start_id, stop_id, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == False and r.record_id > (
                        start_id-1) and r.record_id <= stop_id and r.category.upper() == category.upper())[:]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == False and r.record_id > (
                        start_id-1) and r.record_id <= stop_id)[:]
        return records

    @staticmethod
    def show_first_n(chat_id, n, category):
        with db_session:
            if category:
                records = Record.select(lambda r: r.chat_id == chat_id and r.category.upper() == category.upper())[:n]
            else:
                records = Record.select(lambda r: r.chat_id == chat_id)[:n]
        return records

    @staticmethod
    def show_first_n_active(chat_id, n, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == True and r.category.upper() == category.upper())[:n]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == True)[:n]
        return records

    @staticmethod
    def show_first_n_inactive(chat_id, n, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == False and r.category.upper() == category.upper())[:n]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == False)[:n]
        return records

    @staticmethod
    def show_last_n(chat_id, n, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.category.upper() == category.upper()).order_by(desc(Record.id))[:n]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id).order_by(desc(Record.id))[:n]
        return records

    @staticmethod
    def show_last_n_active(chat_id, n, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == True and r.category.upper() == category.upper()).order_by(
                        desc(Record.id))[:n]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == True).order_by(
                        desc(Record.id))[:n]
        return records

    @staticmethod
    def show_last_n_inactive(chat_id, n, category):
        with db_session:
            if category:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == False and r.category.upper() == category.upper()).order_by(
                        desc(Record.id))[:n]
            else:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.active == False).order_by(
                        desc(Record.id))[:n]
        return records

    @staticmethod
    def show_one_by_id(chat_id, id):
        with db_session:
            record = Record.get(record_id=id, chat_id=chat_id)
        return record

    @staticmethod
    def change_category(chat_id, old_category, new_category):
        try:
            with db_session:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.category == old_category)[:]
                for record in records:
                    with db_session:
                        record.category = new_category
        except Exception:
            logging.exception(
                "ERROR IN MASS CHANGE CATEGORY PROCESS FOR CHAT_ID: {0} AND CATEGORY: {1}".format(
                    chat_id, old_category))
        return True

    @staticmethod
    def edit_category(chat_id, record_id, category):
        try:
            with db_session:
                record = Record.get(record_id=record_id, chat_id=chat_id)
                record.category = category
        except Exception:
            logging.exception(
                "ERROR IN SINGLE EDIT CATEGORY PROCESS FOR CHAT_ID: {0} AND RECORD_ID: {1}".format(
                    chat_id, record_id))
        return record

    @staticmethod
    def delete_category(chat_id, category):
        try:
            with db_session:
                records = Record.select(
                    lambda r: r.chat_id == chat_id and r.category.upper() == category.upper())[:]
                if len(records):    
                    for record in records:
                        record.delete()
                    return True
                else:
                    return False
        except Exception:
            logging.exception(
                "ERROR IN DELETE CATEGORY PROCESS FOR CHAT_ID: {0} AND CATEGORY: {1}".format(
                    chat_id, category))
        return True

    @staticmethod
    def get_all_categories(chat_id):
        with db_session:
            categories = select(r.category for r in Record if r.chat_id == chat_id).distinct()[:]
        if '' in categories:
            categories.remove('')
        return categories
