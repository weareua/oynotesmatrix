import re
from matrix_client.user import User

class Engine():
    """
    Creates engine object and proceed essential methods
    """
    category = ''
    text = ''
    room = None
    sender_name = ''
    chat_id = ''
    
    def prepare_records_list_message(
        self, records, mark_completed=False, keyword=''):
        message = ''
        for record in records:
            # add category marker for general records output
            if len(record.category):
                message += "<b>{0}:</b> ".format(record.category)
            if mark_completed:
                if record.active is False:
                    message += "<i>[Виконано]</i> "
            if len(keyword):
                hihglighted_text = record.text.replace(keyword, "<b>{0}</b>".format(keyword))
                message += '<b>{0}.</b> {1} <br>'.format(record.record_id, hihglighted_text)
            else:
                message += '<b>{0}.</b> {1} <br>'.format(record.record_id, record.text)
        return message

    def prepare_categories_list_message(self, categories):
        message = ''
        for cat in categories:
            message += '<b>{0}</b> <br>'.format(cat)
        return message

    # check on every request. Returns text and category if exists
    def prepare_incoming_data(self, room, event):
        self.text = event['content']['body']
        self.room = room
        self.chat_id = event['room_id']
        self.sender_name = event['sender']

        if re.match(r'(?i)({Cyrillic}\d+|\S+)\s*(.*)', self.text):
            marker = re.match(r'(?i)({Cyrillic}\d+|\S+)\s*(.*)', self.text).groups()[0]
            if marker[0] in ['+', '?', '!', '*', '=', '-']:
                pass
            else:
                self.category, self.text = re.match(r'(?i)({Cyrillic}\d+|\S+)\s*(.*)', self.text).groups()

    def send(self, message):
        """
        Send proceeded message
        """
        if message is '':
            message = 'Жодних записів за цим запитом.'

        self.room.send_html(message)
         
        # handle big messages:
        # message_list = []
        # while len(message) > 4095:
        #     message_list.append(message[0:4095])
        #     message = message[4095:]
        # message_list.append(message)
        # message_list.reverse()

        # while len(message_list) > 0:
        #     self.bot.sendMessage(
        #         chat_id=self.chat_id, text=message_list.pop(), parse_mode=telegram.ParseMode.HTML)
